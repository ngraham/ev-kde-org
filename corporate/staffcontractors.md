---
title: Staff and Contractors
layout: page
menu_active: Organization
staffcontractors:
  - name: Adam Szopa
    title: Project Coordinator (contractor)
    email: adam.szopa<span>@</span>kde.org
    description: Adam started working for KDE in 2020. Adam mainly helps coordinate work related to the KDE Goals, Akademy and other parts of the Community. He has a masters degree in computer science and likes to relax by playing video games. He lives in Poland.    
    image: /corporate/pictures/adam.jpg
  - name: Aniqa Khokhar
    title: Marketing Consultant (contractor)
    email: aniqa.khokhar<span>@</span>kde.org
    description: Aniqa Khokhar is a Marketing Consultant and started working for KDE e.V. in 2020. She contributes to the KDE Promo team activities and strengthens marketing efforts for both the Community and the organization. She has experience in managing marketing campaigns, social media, events, customers, business planning, corporate communications, and market research in education and non-profit sectors. In her spare time, she loves cooking, gardening, and travelling.
    image: /corporate/pictures/aniqa.jpg
  - name: Dina Nouskali
    title: Event Organiser (contractor)
    email: dina.nouskali<span>@</span>kde.org
    description: Dina Nouskali is an Event Organiser and Digital Marketer, who started cooperating with KDE in 2022. She has a Bachelor in Marketing and Advertising and a Master in E-business and Digital Marketing and a big passion for Event Organising. During the last 10 years she has cooperated with different companies and organisations in organising conferences, cultural events, festivals and workshops.
    image: /corporate/pictures/unknown.png
  - name: Ingo Klöcker
    title: App Stores Support Engineer (contractor)
    email: kloecker<span>@</span>kde.org
    description: Ingo started working as App Stores Support Engineer for KDE e.V. in September 2022, but has been a part of KDE since 2000. He supports the KDE community in publishing their applications in the different app stores, so that people on all platforms can enjoy those applications. As freelancing software engineer he improves Kleopatra since mid 2020, and in the 00's he has maintained KMail.
    image: /corporate/pictures/ingo.jpg
  - name: Joseph P. De Veaugh-Geiss
    title: Project and Community Manager
    email: joseph<span>@</span>kde.org
    description: Joseph P. De Veaugh-Geiss is the project and community manager of KDE e.V.'s "Blauer Engel For FOSS" project since July 2021. He supports the project by collecting, summarizing, and spreading information about Blauer Engel eco-certification specifically and resource efficiency in general as it relates to free software development. Prior to working for KDE e.V. he completed a PhD in theoretical and experimental linguistics. When not advocating for free and environmentally-sustainable software, he enjoys learning about languages, listening to music and cooking, and spending as much time as possible under Berlin skies at Tempelhofer Feld.
    image: /corporate/pictures/joseph.jpg
  - name: Lana Lutz
    title: Project Lead and Event Manager
    email: lana.lutz<span>@</span>kde.org
    description: Lana Lutz joined KDE e.V. to support "Blauer Engel for FOSS" as a project lead. She is a creative brand specialist and strategic designer, currently working on her businesses in games and sports nutrition. In the past years, she has worked with startups in design, product development, and marketing. She is passionate about sustainability, nutrition, and design.
    image: /corporate/pictures/lana_sq.jpg
  - name: Paul Brown
    title: Marketing Consultant (contractor)
    email: paul.brown<span>@</span>kde.org
    description: Paul Brown started working as Marketing Consultant for KDE e.V. in 2017. He comes from the world of publishing and has been a Free Software advocate since 1996. He works with the members of the Promo team setting goals, managing campaigns and analysing results. He also helps KDE projects optimise their communication strategies and copywrites and proofreads their websites and blog posts. In his spare time, he enjoys 3D printing, writing tutorials on Free Software usage and articles on writing and communication. He also "enjoys" watching TV series and movies and then ranting about how the creators are "lazy writers" on Reddit and Twitter.
    image: /corporate/pictures/paul.png
  - name: Petra Gillert
    title: Assistant to the Board
    email: petra<span>@</span>kde.org
    description: Petra Gillert is the assistant of KDE e.V.'s board since 2015. She supports the board and the organisation in all matters of organisation and finance and manages the office in Berlin. She ensures continuity in the organisation. In her spare time she enjoys bird watching.
    image: /corporate/pictures/petra.png
# Set this to false if there are no open positions; otherwise, use a list
openpositions:
  - title: Documentation Writer
    description: KDE e.V. is looking for a person with technical writer experience to update KDE‘s documentation across its various projects. Documentation for our software and community is constantly evolving and requires careful planning and maintenance, as well as regular communication and close collaboration with our contributor teams. You will be building on the outcome of previous documentation work.
  - title: Hardware Integrator
    description: Your task will range from discussing with manufacturers and evaluating the kind of hardware they are planning to ship, to making sure that it's viable for KDE to support it either by implementing the features or by coordinating with the different teams to make sure this can happen in a timely manner. This will eventually require collaborating with the system integration communities to ensure our users are getting an optimal experience. Please see the [call for proposals](/resources/callforproposals-hardware2021.pdf) for more details about this contracting opportunity.
---

KDE e.V. supports the KDE Community in various ways, one of them is through its staff and contractors to work on key areas.


{% if page.openpositions %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="/corporate/pictures/open.png" width="60" height="60" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">Open Positions</h3>
    <p>KDE e.V. is hiring! See our <a href="#open-positions">open positions</a> down below for what's available.</p>
  </div>
</div>
{% endif %}

## Current staff and contractors

{% for people in page.staffcontractors %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="{{ people.image }}" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.name }}</h3>
    <p class="people-title">{{ people.title }}</p>
    <p class="people-email">{{ people.email }}</p>
    <p>{{ people.description }}</p>
  </div>
</div>
{% endfor %}

## Open positions

{% if page.openpositions %}
{% for people in page.openpositions %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="/corporate/pictures/open.png" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.title }}</h3>
    <p>{{ people.description | markdownify }}</p>
  </div>
</div>
{% endfor %}
{% else %}
There are no open positions at the moment.
{% endif %}

You are welcome to contact kde-ev-board<span>@</span>kde.org with questions.
