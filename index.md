---
title: "KDE e.V. Homepage"
layout: page
---

<img src="images/ev_large.png" class="float-right img-fluid" style="width: 395px; height: auto;" alt="KDE e.V. logo" />

KDE e.V. is a registered non-profit organization that represents
the <a href="https://kde.org">KDE Community</a> in legal and financial matters.
Read our <a href="/whatiskdeev/">mission statement</a>.


On this site you will find [reports](/reports) about past and
[ongoing activities](/activities)  and information how you can
[get involved](/getinvolved).

KDE e.V. is made up of its [members](/members/), from which the
[board of KDE e.V.](/corporate/board/) is elected.  KDE e.V. has
[supporting members](/supporting-members/) who provide the material
support to carry out KDE e.V.'s activities.

<p>You can find official documents like the
<a href="/corporate/statutes/">articles of association</a>, additional
<a href="/rules">rules and policies</a> or
<a href="/resources">forms</a> for activities as well.</p>

## Recent News

{% for post in site.posts limit: 4 %}
<div class="mb-4">
  <h3 class="mb-0">{{ post.title }}</h3>
  <div class="mb-2"><small>{{ post.date | date: "%Y-%m-%d" }}</small></div>
  {% if post.noquote %}<p>{% else %}<q>{% endif %}
  {{ post.excerpt | strip_html | truncatewords:30 }}
  {% if post.noquote %}</p>{% else %}</q>{% endif %}
  <a href="{{ post.url }}" class="learn-more">{% if site.read_full %}{{ site.read_full }}{% else %}Read full announcement{% endif %}</a>
</div>
{% endfor %}


## Quick Links

<div class="row mb-3">
  <div class="col-12 col-sm-6">
    <div><a href="/contact/">Contact KDE e.V.</a></div>
    <div><a href="/corporate/board/">Board of Directors</a></div>
    <div><a href="/corporate/statutes/">Statutes of Association</a></div>
    <div><a href="/whatiskdeev/">Mission Statement</a></div>
  </div>
  
  <div class="col-12 col-sm-6">
    <div><a href="/reports/">Reports</a></div>
    <div><a href="/resources/supporting_member_application.pdf">New Supporting Member</a></div>
    <div><a href="/resources/ev-questionnaire.text">New Member</a></div>
    <div><a href="/members/">Current Members</a></div>
  </div>
</div>

<a href="https://jointhegame.kde.org"><img src="images/Jtg.png" class="noborder img-fluid" alt="Join the Game and support KDE" /></a>
