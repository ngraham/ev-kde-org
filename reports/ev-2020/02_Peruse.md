<figure class="image-right"> <img width="50%" src="images/Projects/peruse.jpg" alt="Peruse" /><figcaption>Peruse.</figcaption></figure>

Creating rich digital comic books has long been something of a chore, or required the use of proprietary formats. But the Advanced Comic Book Format aims to fix this by introducing concepts like frame based navigation to go with the more traditional page based methods, reference information such as character information, location notes, and so on, all contained within the book, as well as embedded typesetting information and rich text overlays so translation can be done without requiring multiple images of the same page. [Peruse Creator](https://apps.kde.org/perusecreator/) allows you to create these highly interactive and advanced books in a way that is both easy to use and powerful.

### Features

Supports Comic Book Archive formats (cbz, cbr, cb7, cbt, cba)
