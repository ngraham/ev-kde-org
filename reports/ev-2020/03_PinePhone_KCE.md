<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/Products/pinephonekdeedition.png"><img src="images/Products/pinephonekdeedition.png" width="100%" /></a></figure>
</div>

[KDE](https://kde.org/) and [Pine64](https://www.pine64.org/) announced in December 2020 the imminent availability of the [PinePhone - KDE Community edition](https://www.pine64.org/2020/12/01/kde-community-edition-is-now-available/). This Pine64 PinePhone provided developers, technophiles and early adopters a taste of where free mobile devices and software platforms are headed.

The PinePhone - KDE Community edition included most of the essential features a smartphone user would expect and its functionalities increased (and continues to increase) day by day. You can follow the progress of the development of apps and features in the [Plasma Mobile blog](https://www.plasma-mobile.org/blog/).

[Plasma Mobile](https://www.plasma-mobile.org/) is a direct descendant from KDE’s successful [Plasma desktop](https://kde.org/plasma-desktop). The same underlying technologies drive both environments and apps like [KDE Connect](https://kdeconnect.kde.org/) (that lets you connect phones and desktops), the [Okular](https://okular.kde.org/) document reader, the [VVave](https://vvave.kde.org/) music player, and many other programs and utilities, are available on both desktop and mobile.

Thanks to projects like [Kirigami](https://develop.kde.org/frameworks/kirigami//) and [Maui](https://mauikit.org/), developers can write apps that, not only run in multiple environments, but that also gracefully adapt by growing into landscape format when displayed on workstation screen and shrinking to portrait mode on phones. Developers are rapidly populating Plasma Mobile with essential programs, such as web browsers, clocks, calendars, weather apps and games, all of which are being deployed on all platforms, regardless of the layout.

The idea of having mobile devices that can display a full workstation desktop when connected to a monitor, keyboard and mouse, has been around for years and both the KDE Community and Pine64 worked on this concept to make it a reality. The PinePhone handset itself is also ready for convergence. It can use any USB-C dock to connect it to extra USB devices (mouse, keyboard, storage), external monitors or even to a wired network. In fact, the 3GB version of the PinePhone already was shipped with such a dock that provided two extra USB ports, a full-sized HD video port and an RJ45 port.

Talking of hardware, the PinePhone’s kill switches, located under the back cover and above the removable battery, allow users to deactivate the modem, WiFi/Bluetooth, microphone and cameras, and are especially designed to help preserve the owner's privacy. This matches very well KDE’s Community vision of “[a] world in which everyone has control over their digital life and enjoys freedom and privacy”.

The PinePhone - KDE Community edition was launched at a reasonably priced, allowing buyers to enjoy a device on which they can run Plasma Mobile during the next years and allowed owners to test and mold with their feedback the mobile system of the future.

Parties interested in developing, got early access to an ambitious Free Software mobile platform and helped contribute to Plasma Mobile and get a headway designing new apps that can be tested and deployed immediately.

### Specs

* Allwinner A64 Quad Core SoC with Mali 400 MP2 GPU
* 2GB/3GB of LPDDR3 RAM
* 5.95″ LCD 1440×720, 18:9 aspect ratio (hardened glass)
* Bootable micro SD
* 16GB/32GB eMMC
* HD Digital Video Out
* USB Type C (Power, Data and Video Out)
* Quectel EG-25G with worldwide bands
* WiFi: 802.11 b/g/n, single-band, hotspot capable
* Bluetooth: 4.0, A2DP
* GNSS: GPS, GPS-A, GLONASS
* Vibrator
* RGB status LED
* Selfie and Main camera (2/5Mpx respectively)
* Main Camera: Single OV6540, 5MP, 1/4″, LED Flash
* Selfie Camera: Single GC2035, 2MP, f/2.8, 1/5″
* Sensors: accelerator, gyro, proximity, compass, barometer, ambient light
* 3 External Switches: up, down and power
* HW switches: LTE/GNSS, WiFi, Microphone, Speaker, Cameras
* Samsung J7 form-factor 3000mAh battery
* The case is matte black finished plastic
* Headphone Jack
