---
title: 'On the reappointment of Richard Stallman as a director of the Free Software Foundation (FSF)'
date: 2021-03-24 21:06:00 
layout: post
---

This week, the reappointment of Richard Stallman to the board of directors of the Free Software Foundation (FSF) has caused us significant concern about the governance of the organization.

The Free Software Foundation (FSF) has a broad mission to perform representational work on behalf of Free Software communities. It is also tasked with the ongoing maintenance of Free Software licenses used by KDE and many other Free Software communities. For these reasons the health and structure of the organization are of concern to the KDE community.

In KDE e.V., directors are appointed by the full membership, by majority vote. Changes in the composition of our board of directors are participatory and transparent. Today we have doubts that corresponding processes within the FSF hold up to this common standard, which we believe to be critical to functional, sustainable governance given 23 years of our own experience as an organization.

Organized activity in the wider Free Software community continues to be important. Safe participation and representation are necessary ingredients, along with inclusivity. Richard Stallman's conduct and the actions of the FSF board of directors have been harmful to all of them.

KDE e.V. will stay involved in the discussion, with our partners and others, on where to take Free Software next.

*This statement was updated on 2021-03-25 to clarify the connection to the KDE community's standards of conduct and inclusiveness.*
